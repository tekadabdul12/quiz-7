<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_File Uploader_file-submit</name>
   <tag></tag>
   <elementGuidId>fab86575-1e71-4e6b-8856-f23e21aba5f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='file-submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#file-submit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ac215cb4-b2ba-4e35-b4cc-35220230ee6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a511c472-2667-46da-95c3-d1193a9ce60f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>file-submit</value>
      <webElementGuid>46e9f502-1007-4667-a184-6084936f3648</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>2f16aa4d-d5d9-4ba9-bce1-39ecff40379e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Upload</value>
      <webElementGuid>31710595-6921-47f0-a7fc-b8dceabddcae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;file-submit&quot;)</value>
      <webElementGuid>b3f6c7c9-44e0-4360-980c-dcf5f10cff9b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='file-submit']</value>
      <webElementGuid>89f00756-892b-4264-bc71-8e385f5555fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/form/input[2]</value>
      <webElementGuid>48458348-511c-492c-93dd-6864b126766e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='File Uploader'])[1]/following::input[2]</value>
      <webElementGuid>20bdb50e-0aa5-4828-b371-16c3ea29c666</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✔'])[1]/preceding::input[1]</value>
      <webElementGuid>059cd785-5ac7-4c94-82fd-18d84a4fd0fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input[2]</value>
      <webElementGuid>44a9e247-78bf-475c-8b92-2a3d2268fd56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'file-submit' and @type = 'submit']</value>
      <webElementGuid>591f680b-d2a8-44b2-8047-1c355a27f770</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
